//
//  RKScoreService.swift
//  RKApi
//
//  Created by Bruno Isola Reginato on 21/01/17.
//  Copyright © 2017 BReginato. All rights reserved.
//

import Foundation


public typealias RKScoreCreateCompletion = ([String: Any?]?,Error?) -> (Void)

@objc public class RKScoreService : NSObject {
    override public init() {
        super.init()
    }
    
    public func send(score: String, forUserWithEmail email: String, andAppToken token:String, andCompletion completion:@escaping RKScoreCreateCompletion) {
        let kEndpointName = "scores/track"
        
        var endpointURL = URL(string:  RKConstants.kBaseURLString)
        endpointURL!.appendPathComponent(kEndpointName)
        
        //Making the request
        var request = URLRequest(url: endpointURL!)
        request.httpMethod = "POST"
        request.addValue(token, forHTTPHeaderField: RKConstants.kAppTokenHeaderKey)
        
        let params:[String:Any] = ["score": score, "user": ["login": email]]
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        
        //Making the call
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                return completion(nil, error)
            }
            
            guard let safeData = data else {
                return completion(nil, error)
            }
            
            do {
                guard let resp = try? JSONSerialization.jsonObject(with: safeData, options: []) as? [String:Any?] else {
                    throw RKGenericError.Parse
                }
                
                //success
                completion(resp,nil)
            } catch let error {
                return completion(nil, error)
            }
            
            }.resume()
        
    }
}

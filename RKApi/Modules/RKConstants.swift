//
//  RKConstants.swift
//  RKApi
//
//  Created by Bruno Isola Reginato on 21/01/17.
//  Copyright © 2017 BReginato. All rights reserved.
//

import Foundation

public struct RKConstants {
    public static let kBaseURLString = "http://www.rankeado.com.br/core/api/public"
    public static let kAppTokenHeaderKey = "x-app-token"
}

public enum RKGenericError: Error {
    case Parse
}

//
//  RKRankingService.swift
//  RKApi
//
//  Created by Bruno Isola Reginato on 22/01/17.
//  Copyright © 2017 BReginato. All rights reserved.
//
import Foundation

public typealias RKRankingFromUserCompletion = ([String: Any?]?,Error?) -> (Void)
public typealias RKRankingFromAppCompletion = ([String: Any?]?,Error?) -> (Void)

@objc public class RKRankingService : NSObject {
    public override init() {
        super.init()
    }

    public func rankingFromUserWith(email: String, andAppToken token:String, andCompletion completion:@escaping RKRankingFromUserCompletion) {
        let kEndpointName = "ranking/score"
        let kUserLoginHeaderKeyName = "x-user-login"
        
        var endpointURL = URL(string:  RKConstants.kBaseURLString)
        endpointURL!.appendPathComponent(kEndpointName)
        
        //Making the request
        var request = URLRequest(url: endpointURL!)
        request.httpMethod = "GET"
        request.addValue(token, forHTTPHeaderField: RKConstants.kAppTokenHeaderKey)
        request.addValue(email, forHTTPHeaderField: kUserLoginHeaderKeyName)
        
        
        //Making the call
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                return completion(nil, error)
            }
            
            guard let safeData = data else {
                return completion(nil, error)
            }
            
            do {
                guard let resp = try? JSONSerialization.jsonObject(with: safeData, options: []) as? [String:Any?] else {
                    throw RKGenericError.Parse
                }
                
                //success
                completion(resp,nil)
            } catch let error {
                return completion(nil, error)
            }
            
            }.resume()
        
    }
    
    public func rankingFromAppWith(token:String, andCompletion completion:@escaping RKRankingFromAppCompletion) {
        let kEndpointName = "ranking/app/score"
        
        var endpointURL = URL(string:  RKConstants.kBaseURLString)
        endpointURL!.appendPathComponent(kEndpointName)
        
        //Making the request
        var request = URLRequest(url: endpointURL!)
        request.httpMethod = "GET"
        request.addValue(token, forHTTPHeaderField: RKConstants.kAppTokenHeaderKey)
        
        
        //Making the call
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                return completion(nil, error)
            }
            
            guard let safeData = data else {
                return completion(nil, error)
            }
            
            do {
                guard let resp = try JSONSerialization.jsonObject(with: safeData, options: []) as? [String:Any?] else {
                    throw RKGenericError.Parse
                }
                
                //success
                completion(resp,nil)
            } catch let error {
                return completion(nil, error)
            }
            
            }.resume()
        
    }
}

//
//  RKUserService.swift
//  RKApi
//
//  Created by Bruno Isola Reginato on 21/01/17.
//  Copyright © 2017 BReginato. All rights reserved.
//

import Foundation

public typealias RKUserCreateCompletion = ([String: Any?]?,Error?) -> (Void)

@objc public class RKUserService : NSObject {
    override public init() {
        super.init()
    }
    
    public func createUserWith(email: String, firstName: String, andCompletion completion:@escaping RKUserCreateCompletion) {
        let kEndpointName = "user/create"
        
        var endpointURL = URL(string:  RKConstants.kBaseURLString)
        endpointURL!.appendPathComponent(kEndpointName)
        
        //Making the request
        var request = URLRequest(url: endpointURL!)
        request.httpMethod = "POST"
        let params = ["email": email, "firstName": firstName]
        request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: .prettyPrinted)
        
        //Making the call
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if error != nil {
                return completion(nil, error)
            }
            
            guard let safeData = data else {
                return completion(nil, error)
            }
            
            do {
                guard let resp = try? JSONSerialization.jsonObject(with: safeData, options: []) as? [String:Any?] else {
                    throw RKGenericError.Parse
                }
                
                //success
                completion(resp,nil)
            } catch let error {
                return completion(nil, error)
            }
            
        }.resume()
        
    }
}

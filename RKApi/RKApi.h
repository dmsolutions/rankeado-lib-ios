//
//  RKApi.h
//  RKApi
//
//  Created by Bruno Isola Reginato on 21/01/17.
//  Copyright © 2017 BReginato. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for RKApi.
FOUNDATION_EXPORT double RKApiVersionNumber;

//! Project version string for RKApi.
FOUNDATION_EXPORT const unsigned char RKApiVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <RKApi/PublicHeader.h>
